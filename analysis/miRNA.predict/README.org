#+TITLE: miRDeep-P Prediction
#+AUTHOR: Tony Arick
#+TODO: BAD TODO | GOOD QUEUE DONE SKIP
#+DRAWERS: HIDDEN
#+OPTIONS: d:RESULTS 
#+STARTUP: hideblocks align

#+PROPERTY:  header-args :exports results :eval never-export
#+OPTIONS: ^:nil 

#+HEADER: :shebang #!/bin/bash :tangle db/build.sh
#+HEADER: :prologue #PBS -N BuildDB -l nodes=1:ppn=20 -l walltime=48:00:00
#+BEGIN_SRC sh
PATH=$PATH:/usr/local/igbb/bowtie-1.1.2/
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$(git rev-parse --show-toplevel)"
swsetup () { eval `/usr/erc-share/etc/swsetup/swsetup.pl $*`; }
swsetup samtools

zcat $BASE/ref/GCF_000006275.2_JCVI-afl1-v2.0_genomic.fna.gz > $DIR/A.flavus.genome.fa
zcat $BASE/ref/GCF_000006275.2_JCVI-afl1-v2.0_genomic.gff.gz > $DIR/A.flavus.annotation.gff3
bowtie-build $DIR/A.flavus.genome{.fa,}
awk '$3 !~ /.*region/'  $DIR/A.flavus.annotation.gff3 |
    sort -k1,1 -k4,4n > $DIR/A.flavus.annotation.clean.gff3
samtools faidx $DIR/A.flavus.genome.fa
cut -f 1,2 $DIR/A.flavus.genome.fa.fai > $DIR/A.flavus.genome

zcat $BASE/ref/GCF_000005005.2_B73_RefGen_v4_genomic.fna.gz > $DIR/Z.mays.genome.fa
zcat $BASE/ref/GCF_000005005.2_B73_RefGen_v4_genomic.gff.gz |
    awk '$3 !~ /.*region/' OFS="\t" | sort -k1,1 -k4,4n > $DIR/Z.mays.annotation.clean.gff3
awk '$3 == "gene"' OFS="\t"  $DIR/Z.mays.annotation.clean.gff3 | sort -k1,1 -k4,4n > $DIR/Z.mays.annotation.genes.gff3
samtools faidx $DIR/Z.mays.genome.fa
cut -f 1,2 $DIR/Z.mays.genome.fa.fai > $DIR/Z.mays.genome
bowtie-build $DIR/Z.mays.genome{.fa,}

#+END_SRC

# Preprocessing reads
#+HEADER: :shebang #!/bin/bash :tangle preprocess/run.sh :mkdirp yes
#+BEGIN_SRC sh :var libs=../../raw/README.org:bgi_trim
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$(git rev-parse --show-toplevel)"

grep -v -e Control -e H2O <<<"$libs" |
    while read -a lib; do
        zcat $BASE/trim/${lib[0]}.filtered.fq.gz | awk 'NR%4==2' | sort | uniq -c 
    done |
    sort -k2,2 |
    awk 'prev != $2 {printf ">SEQ_%08d_x%d\n%s\n", c++, sum, prev; prev = $2; sum = 0}
         prev == $2 {sum += $1}' | sed 1,2d > $DIR/reads.fa
#+END_SRC

# Align
#+HEADER: :shebang #!/bin/bash :tangle align/align.sh :mkdirp yes
#+HEADER: :prologue #PBS -N Trim -l nodes=1:ppn=20 -l walltime=48:00:00 
#+BEGIN_SRC sh :var libs=../../raw/README.org:bgi_trim[,0]
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

  PATH=$PATH:/usr/local/igbb/bedtools-2.25.0/bin/
  PATH=$PATH:/usr/local/igbb/bowtie-1.1.2/
  PATH=$PATH:$DIR/software/miRDP1.3/

  swsetup () { eval `/usr/erc-share/etc/swsetup/swsetup.pl $*`; }
  swsetup samtools

  DB=$DIR/../db/A.flavus.genome
  GFF=$DIR/../db/A.flavus.annotation.clean.gff3
  READS=$DIR/../preprocess

  bowtie -p $PBS_NUM_PPN -S -a -v 0 -m 30 $DB       \
         -f $READS/reads.fa 2> >(tee $DIR/reads.stats >&2) |
      samtools view -b -F 4 - |
      samtools sort -T $DIR/reads.tmp -O bam - | 
  bedtools intersect -v -a /dev/stdin -b $GFF |
      samtools view -b - > $DIR/reads.bam

#+END_SRC

# Filter 
#+HEADER: :shebang #!/bin/bash :tangle filter.sh :mkdirp yes
#+HEADER: :prologue #PBS -N Trim -l nodes=1:ppn=20 -l walltime=48:00:00 -t 1-24
#+BEGIN_SRC sh :var libs=../../raw/README.org:bgi_trim[,0]
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PATH=$PATH:/usr/local/igbb/bedtools-2.25.0/bin/
PATH=$PATH:/usr/local/igbb/bowtie-1.1.2/
PATH=$PATH:$DIR/software/miRDP1.3/

swsetup () { eval `/usr/erc-share/etc/swsetup/swsetup.pl $*`; }
swsetup samtools

bedtools bamtobed -i $DIR/align/reads.bam | 
    sort -k1,1 -k2,2n -T $DIR |
    bedtools merge -d 30 -i stdin -s > $DIR/align/combined.filter.bed



#+END_SRC

# Find precursors
#+HEADER: :shebang #!/bin/bash :tangle precursors.sh :mkdirp yes
#+HEADER: :prologue #PBS -N Precursors -l nodes=1:ppn=20 -l walltime=48:00:00
#+BEGIN_SRC sh :var libs=../../raw/README.org:bgi_trim[,0]
PATH=$PATH:/usr/local/igbb/bedtools-2.25.0/bin/
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# bedtools bamtobed -i $DIR/align/reads.bam | 
#     sort -k1,1 -k2,2n -T $DIR |
#     bedtools merge -d 30 -i stdin -s |
#     bedtools intersect -s -v -a /dev/stdin -b $DIR/align/combined.filter.bed |
    sort -k4,4r -k1,1 -k2,2n  -T $DIR $DIR/align/combined.filter.bed |
    awk '$3-$2 <= 30 {tmp = $3;
                        $3=$2+205;
                        print;
                        $3=tmp;
                        $2=($3>=205?$3-205:0);}
                        {print}' OFS="\t" |
    bedtools slop -b 22 -i - -g $DIR/db/A.flavus.genome |
    awk '$3-$2 <= 280' |
    awk '{printf "%s\t%d\t%d\t%s_%d strand:%s excise_beg:%d excise_end:%d\t0\t%s\n",
                     $1,$2,$3,$1,NR-1,$4,$2+1,$3,$4}' |
    bedtools getfasta -fi $DIR/db/A.flavus.genome.fa -bed - -fo - -name -s \
             >  $DIR/precursors.combined.fa
#+END_SRC

#+HEADER: :shebang #!/bin/bash :tangle structures.sh :mkdirp yes
#+HEADER: :prologue #PBS -N Predict_Structures -l nodes=1:ppn=20 -l walltime=48:00:00
#+BEGIN_SRC sh
  PATH=$PATH:/usr/local/igbb/ViennaRNA-2.2.5/bin/:/usr/local/gnu/bin
  LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/igbb/ViennaRNA-2.2.5/lib64/
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

  cat $DIR/precursors.combined.fa |
      parallel  --block 1m --pipe --recstart ">" RNAfold --noPS > $DIR/predicted_structures.combined
#+END_SRC

#+RESULTS:


#+HEADER: :shebang #!/bin/bash :tangle signatures.sh :mkdirp yes
#+HEADER: :prologue #PBS -N Predict_Signatures -l nodes=1:ppn=20 -l walltime=48:00:00
#+BEGIN_SRC sh :var libs=../../raw/README.org:bgi_trim[,0]
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
swsetup () { eval `/usr/erc-share/etc/swsetup/swsetup.pl $*`; }

PATH=$PATH:/usr/local/igbb/bowtie-1.1.2/
PATH=$PATH:/usr/local/igbb/bedtools-2.25.0/bin/
PATH=$PATH:/usr/local/igbb/miRDP1.3/
swsetup samtools

bowtie-build --large-index $DIR/precursors.combined.fa $DIR/db/precursors.combined

samtools view $DIR/align/reads.bam |
    awk '!_[$1]++ {printf ">%s\n%s\n", $1, $10}' > $DIR/sig.combined.fa

bowtie --large-index -p$PBS_NUM_PPN -a -v 0 $DIR/db/precursors.combined -f $DIR/sig.combined.fa |
    convert_bowtie_to_blast.pl /dev/stdin \
                               $DIR/sig.combined.fa \
                               $DIR/precursors.combined.fa |
    sort +3 -25  > $DIR/predicted_signatures.combined
#+END_SRC

#+HEADER: :shebang #!/bin/bash :tangle predict.sh :mkdirp yes
#+HEADER: :prologue #PBS -N Predict_miRNA -l nodes=1:ppn=1 -l walltime=48:00:00
#+BEGIN_SRC sh
PATH=$PATH:/usr/local/igbb/miRDP1.3/
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

miRDP.pl $DIR/predicted_{signatures,structures}.combined > $DIR/predicted_miRNA.combined
rm_redundant_meet_plant.pl $DIR/db/A.flavus.genome $DIR/precursors.combined.fa $DIR/predicted_miRNA{,.nr,.p}.combined
#+END_SRC

#+HEADER: :shebang #!/bin/bash :tangle name.sh :mkdirp yes
#+HEADER: :prologue #PBS -N Combine_and_Name -l nodes=1:ppn=1 -l walltime=48:00:00
#+BEGIN_SRC sh 
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

  comm <(sort $DIR/predicted_miRNA.nr) <(sort $DIR/predicted_miRNA.p) |
      sed 's/^\t\t\(.*\)/\1\tyes/' |
      sort -k9,9r |
      awk 'BEGIN{OFS="\t"; print "novel miRNA",
                                 "Chr",
                                 "Strand",
                                 "Read",
                                 "Precursor",
                                 "mature miRNA location",
                                 "precursor location",
                                 "mature sequence",
                                 "precursor sequence",
                                 "Passed Plant Criteria"
                }
           { print "Predict-miR-" NR, $0 }' \
          > $DIR/named.predicted_miRNA.txt
#+END_SRC

#+RESULTS:


#+HEADER: :shebang #!/bin/bash :tangle targets.sh :mkdirp yes
#+HEADER: :prologue #PBS -N Predict_Targets -l nodes=1:ppn=1 -l walltime=48:00:00
#+BEGIN_SRC sh :var DIR=(file-name-directory buffer-file-name)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

swsetup () { eval `/usr/erc-share/etc/swsetup/swsetup.pl $*`; }

PATH=$PATH:/usr/local/igbb/bowtie-1.1.2/
PATH=$PATH:/usr/local/igbb/bedtools-2.25.0/bin/
PATH=$PATH:/usr/local/igbb/miRDP1.3/
swsetup samtools

awk '{printf ">%s\n%s\n", $1, $8}' $DIR/named.predicted_miRNA.txt |
    bowtie -S -p $PBS_NUM_PPN -a -v 0 $DIR/db/Z.mays.genome -f /dev/stdin |
    samtools view -bS -F4 - > $DIR/Aflavus_miRNA_vs_Zmays_genome.bam

bedtools bamtobed -i $DIR/Aflavus_miRNA_vs_Zmays_genome.bam |
    sort -k1,1 -k2,2n |
    bedtools closest -S -D b -a stdin -b $DIR/db/Z.mays.annotation.genes.gff3 |
    awk '($16 >=0 && $16<=500) || ($16 <= 0 && $16 >= -500)' OFS="\t" FS="\t" > $DIR/targets.gff
    # cut -f 4,15,16 |
    # sed 's/[^\t]*Parent=evm.model.//' |
    # uniq |
    # sort -k2,2 |
    # join -t$'\t' -1 2 -a 1 -2 1 -o '1.1 1.2 2.3 1.3' - <(sort -k1,1 db/NBI_TM1-annotation.csv) |
    # sort -k1,1 |
    # awk 'prev == $1 { printf ";%s|distance:%d|%s", $2,$4,$3}
    #        prev != $1 {printf "\n%s\t%s|distance:%d|%s", $1,$2,$4,$3; prev = $1;}' FS="\t" |
    # sort -k 1,1 -t$'\t'   |
    # awk 'NR == 1 {$0 =  "novel miRNA\ttargets"} {print}' |
    # join -t$'\t' -a 1 -1 1 -2 1 <(sort -k1,1 analysis/miRDP/named.predicted_miRNA.txt) - |
    # sort -t'-' -k3,3n > analysis/miRDP/named.predicted_miRNA.targets_added.txt
#+END_SRC

#+RESULTS:

#+BEGIN_SRC sh
PATH=$PATH:/work/maa146/ma.q136.annex/bin

cut -f 7 predicted_miRNA.p.combined | awk '{printf ">miR%d\n%s\n", NR, $0}' > predicted_miRNA.fa
miranda predicted_miRNA.fa db/Z.mays.genome.fa

#+END_SRC

