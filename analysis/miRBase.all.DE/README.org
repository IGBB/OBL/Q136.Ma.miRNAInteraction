#+TITLE: Differential Expression
#+AUTHOR: Adam Thrash
#+TODO: BAD TODO | GOOD QUEUE DONE SKIP
#+DRAWERS: HIDDEN
#+OPTIONS: d:RESULTS 
#+STARTUP: hideblocks align

#+PROPERTY:  header-args :exports results :eval never-export
#+OPTIONS: ^:nil 

# Get miRBase data (release 21) and set up folders
#+BEGIN_SRC sh
mkdir db; mkdir counts; mkdir bwa; mkdir edgeR
cd db;
wget ftp://mirbase.org/pub/mirbase/CURRENT/mature.fa.gz
gunzip mature.fa.gz
#+END_SRC

# Index miRBase for bwa alignment
#+BEGIN_SRC sh :results none
PATH=$PATH:/usr/local/igbb/bwa/bin/
cd db/
bwa index mature.fa
#+END_SRC

# Align reads to mirBase
#+HEADER: :shebang #!/bin/bash :tangle align.sh :mkdirp yes
#+BEGIN_SRC sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PATH=$PATH:/usr/local/igbb/bwa/bin/:/usr/local/gnu/bin/:/usr/local/igbb/samtools/bin/

cd $DIR/
echo $DIR/

for file in ../../trim/*filtered.fq.gz; do
    echo "$(basename $file .filtered.fq.gz)";
    bwa aln -n 1 -o 0 -e 0 -l8 -k 0 -t 20  db/mature.fa $file |
        bwa samse db/mature.fa - $file |
        samtools view -F4 -bS - > "bwa/$(basename $file .filtered.fq.gz).bam";
done
#+END_SRC

# Count alignments to Z. mays
#+HEADER: :shebang #!/bin/bash :tangle count.sh :mkdirp yes
#+BEGIN_SRC sh
for file in bwa/*.bam; do
    samtools view $file | cut -f 3 | sort | uniq -c | awk '{print $2,$1}' OFS="\t" > "counts/$(basename $file .bam)"
done

#+END_SRC

#+NAME:exp_design
| Library         | Group       | Type          |
|-----------------+-------------+---------------|
| Mp719-21882-1   | resistant   | without_toxin |
| Mp719-21882-2   | resistant   | without_toxin |
| Mp719-21882-3   | resistant   | without_toxin |
| Mp719-3357-1    | resistant   | with_toxin    |
| Mp719-3357-2    | resistant   | with_toxin    |
| Mp719-3357-3    | resistant   | with_toxin    |
| Mp719-H2O-1     | resistant   | h20           |
| Mp719-H2O-2     | resistant   | h20           |
| Mp719-H2O-3     | resistant   | h20           |
| Mp719-Control-1 | resistant   | uninoculated  |
| Mp719-Control-2 | resistant   | uninoculated  |
| Mp719-Control-3 | resistant   | uninoculated  |
| Va35-21882-1    | susceptible | without_toxin |
| Va35-21882-2    | susceptible | without_toxin |
| Va35-21882-3    | susceptible | without_toxin |
| Va35-3357-1     | susceptible | with_toxin    |
| Va35-3357-2     | susceptible | with_toxin    |
| Va35-3357-3     | susceptible | with_toxin    |
| Va35-Control-1  | susceptible | uninoculated  |
| Va35-Control-2  | susceptible | uninoculated  |
| Va35-Control-3  | susceptible | uninoculated  |
| Va35-H2O-1      | susceptible | h20           |
| Va35-H2O-2      | susceptible | h20           |
| Va35-H2O-3      | susceptible | h20           |

# Differential expression 
#+HEADER: :shebang "#!/bin/env Rscript --vanilla" :mkdirp t :tangle edgeR_Z_mays.pbs
#+BEGIN_SRC R :var targets=exp_design :results output
library(edgeR)
library(methods)
library(scales)
library(ggplot2)

# targets<-read.delim('targets.txt', header=TRUE)

targets$files = targets$Library
count.dir = "counts/"

design <- model.matrix(~-1 + Group:Type, data=targets)
colnames(design) <- make.names(gsub('Group|Type', '', colnames(design)))

my.contrasts <- makeContrasts(
    resistant.without_toxin - resistant.with_toxin,
    susceptible.with_toxin - resistant.with_toxin,
    susceptible.h20 - resistant.h20,
    susceptible.without_toxin - susceptible.with_toxin,
    resistant.with_toxin - resistant.without_toxin,
    susceptible.with_toxin - susceptible.without_toxin,
    resistant.with_toxin - susceptible.with_toxin,
    resistant.without_toxin - susceptible.without_toxin,
    susceptible.uninoculated - resistant.uninoculated,
    resistant.with_toxin - resistant.uninoculated,
    susceptible.with_toxin - susceptible.uninoculated,
    resistant.without_toxin - resistant.uninoculated,
    susceptible.without_toxin - susceptible.uninoculated,
    levels=design)

outdir = 'edgeR/';
d <- readDGE(targets$files, path=count.dir)

## Filter Low Expression Genes
keep <- d$counts[rowSums(cpm(d$counts) > 0) > 3,]

d <- DGEList(counts=keep, group=row.names(targets))

y <- estimateGLMCommonDisp(d,design)
y <- estimateGLMTrendedDisp(y,design)
y <- estimateGLMTagwiseDisp(y,design)
fit <- glmFit(y,design)

lrt <- lapply(colnames(my.contrasts), function(x) glmLRT(fit, contrast=my.contrasts[,x]))
names(lrt) <- colnames(my.contrasts)

tables <- lapply(lrt, function(x) topTags(x, n=Inf)$table)

lapply(names(tables), function(x) write.table(tables[[x]], row.names=T, file=paste0(outdir, x, '.de.txt'), sep="\t"))

tmp <- plotMDS(y, top=Inf)
mds <- data.frame(labels=paste(targets$Type, targets$Group, sep='-'), 
                 group =paste(targets$Type, targets$Group, sep='-'), x=tmp$x, y=tmp$y)
hull <- do.call(rbind, lapply(levels(mds$group), function(x) mds[mds$group==x,][chull(mds[mds$group==x,c('x','y')]),]))

ggplot(mds, aes(x=x,y=y,fill=group, color=group)) + 
    geom_polygon(data=hull, aes(x=x,y=y,group=group), alpha=0.33) +
    geom_point()  + 
    theme_bw() + 
    theme(axis.text.x = element_blank(),  # remove x-axis text
          axis.text.y = element_blank(), # remove y-axis text
          axis.ticks = element_blank(),  # remove axis ticks
          axis.title.x = element_blank(), # remove x-axis labels
          axis.title.y = element_blank(), # remove y-axis labels
          panel.background = element_blank(), 
          panel.grid.major = element_blank(),  #remove major-grid labels
          panel.grid.minor = element_blank(),  #remove minor-grid labels
          plot.background = element_blank()) + scale_color_brewer(palette="Paired") + scale_fill_brewer(palette="Paired")
   
ggsave("edgeR/Z.mays.pca.png")

library(ascii)
options(asciiType="org")
ascii(t(sapply(tables, function(x) summary(x$FDR <= 0.05)[2:3])), include.rownames=T, include.colnames=T, header=T, 
      caption="Summary of Differentailly Expressed Genes (FDR adj p <= 0.05)")
#+END_SRC

#+RESULTS:
#+CAPTION: Summary of Differentailly Expressed Genes (FDR adj p <= 0.05)
|                                                      | FALSE | NA's |
|------------------------------------------------------+-------+------|
| resistant.without_toxin - resistant.with_toxin       | 186   | 0    |
| susceptible.with_toxin - resistant.with_toxin        | 184   | 2    |
| susceptible.h20 - resistant.h20                      | 182   | 4    |
| susceptible.without_toxin - susceptible.with_toxin   | 186   | 0    |
| resistant.with_toxin - resistant.without_toxin       | 186   | 0    |
| susceptible.with_toxin - susceptible.without_toxin   | 186   | 0    |
| resistant.with_toxin - susceptible.with_toxin        | 184   | 2    |
| resistant.without_toxin - susceptible.without_toxin  | 184   | 2    |
| susceptible.uninoculated - resistant.uninoculated    | 185   | 1    |
| resistant.with_toxin - resistant.uninoculated        | 165   | 21   |
| susceptible.with_toxin - susceptible.uninoculated    | 182   | 4    |
| resistant.without_toxin - resistant.uninoculated     | 172   | 14   |
| susceptible.without_toxin - susceptible.uninoculated | 179   | 7    |
